#Write your code here
def displayBoard(elem_matrix)
    elem_matrix.to_a.each {|r| puts r.inspect}
end
def turno (jugador, columnas_posibles)
    repetir= true
    puts "Turno de #{jugador}"
    while repetir
        puts "Seleccione una columna para jugar de las siguientes #{columnas_posibles}"
        begin
            jug= gets.chomp
            jug = Integer(jug)
        rescue ArgumentError
            puts "Por favor seleccione una columna válida para jugar de las siguientes #{columnas_posibles}"
            retry
        end
        repetir =  !(columnas_posibles.include?(jug))
        if repetir
            puts "Columna no valida. Seleccione una columna válida"        
        else
            return jug
        end
    end
end
def play(jug, tablero,columnas_posibles, playerNum)    # esta funcion se ejecuta cuando un jugador haga su jugada en la columna seleccionada
    for i in (5).downto(0)
        if tablero[i][jug-1]=='xx'
            tablero[i][jug-1]="J#{playerNum}"
            if i==0
                columnas_posibles.delete(jug)
            end
            break
        end
    end
    return tablero, columnas_posibles
end
def checkRows (tablero, jugador, player_name) # Comprueba si en las filas del tablero existen 4 en linea
    tablero.each do |fila|
        fila_str=fila.join("")
        respuesta=fila_str.include?(jugador*4);
        if (respuesta)
            puts("#{player_name} ha ganado!!!")
            return true
        end
    end
    return false
end
def checkDiagonals(tablero, fila_final, col_final, jugador, player_name) # la ultima fila o columna desde donde se trazara la diagonal dado q tienen q tener minimo 4 elementos en diagonal
    return (checkDiagonal(tablero, fila_final, col_final, jugador, player_name)||checkDiagonal(tablero.reverse.transpose,col_final,fila_final,jugador, player_name))
end
def checkDiagonal(tablero, fila_final, col_final, jugador, player_name) #chequea en diagonal hacia la derecha de arriba a abajo
    diagonales=[]
    if fila_final==2
        limites=[5,6,0]
    else
        limites=[5,5,1]
    end
    for i in 0..fila_final
        str=""
        add=limites[2]
        if (i==0)
            add=0
        end
        for j in 0..limites[0]-i+add
            str=str+tablero[i+j][j]
        end
        diagonales.push(str)
    end
    for i in 1..col_final
        str=""
        for j in 0..limites[1]-i
            str=str+tablero[j][i+j]
        end
        diagonales.push(str)
    end 
    diagonales.each do |string|
        if (string.include?(jugador*4))
            puts("#{player_name} ha ganado!!!")
            return true
        end
    end    
    return false
end
def checkBoard(tablero, jugador, player_name, cant_jugadas) #chequea si ha ganado el usuario que ha jugado o si ha habido empate. Retorna true si el juego se ha acabado
    if (checkRows(tablero, jugador, player_name)||checkRows(tablero.transpose, jugador,player_name)||checkDiagonals(tablero,2,3,jugador,player_name))
        return true 
    end
    if (cant_jugadas==42)
        puts("Es un empate!!!")
        return true
    end
    return false
end
def main
    tablero= Array.new(6) { Array.new(7, 'xx') } # dimensiones del tablero 6 filas y 7 columnas vacias (ceros)  
    columnas_posibles=[1,2,3,4,5,6,7];  
    player1=''
    player2=''
    cant_jugadas=0
    juego_activo=true # Cuando esté en false se acaba el juego
    while player1==''
        puts 'Introduzca el nombre del jugador 1'
        player1 = gets.chomp
    end    
    while player2==''
        puts 'Introduzca el nombre del jugador 2'
        player2 = gets.chomp
    end    
    displayBoard(tablero)
    while juego_activo
        jugada = turno(player1, columnas_posibles)
        tablero, columnas_posibles= play(jugada, tablero, columnas_posibles, 1)
        juego_activo=!checkBoard(tablero, "J1", player1, cant_jugadas)
        displayBoard(tablero)
        if (!juego_activo)
            next
        end    
        jugada = turno(player2, columnas_posibles)
        tablero, columnas_posibles= play(jugada, tablero, columnas_posibles, 2)
        cant_jugadas=cant_jugadas+2
        juego_activo=!checkBoard(tablero, "J2", player2, cant_jugadas)
        displayBoard(tablero)
    end
end

keep_playing=true
while keep_playing
    main
    puts 'Para jugar de nuevo pulse cualquier tecla, para salir pulse 0 (cero)'
    entrada=gets.chomp
    if entrada==='0'
        keep_playing=false
    end
end
